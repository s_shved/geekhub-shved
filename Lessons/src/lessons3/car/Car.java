package lessons3.car;

import java.util.Scanner;

import lessons3.car.exception.NotEnoughFuelException;
import lessons3.car.exception.OverflowGasTankCarException;
import lessons3.car.exception.SpeedingCarException;
import lessons3.car.parts.*;

public class Car implements Driveable {

	GasTank tank = new GasTank();
	Engine engine = new Engine();
	Wheel wheel = new Wheel();
	
	@Override
	public void travel() {
		System.out.println("Enter the distance: ");
		int userDistance = (new Scanner(System.in)).nextInt();
		double distance = 0;
		while (distance < userDistance)
			try {
				distance = distance + wheel.goCar(engine.startEngine(tank.getFuel(1)), wheel.getSpeed());
			} catch (NotEnoughFuelException e) {
				System.out.println("Fuel is over! Press 1 to fill 10 liters, or 2 to end the trip: ");
				int input = new Scanner(System.in).nextInt();
				if(input == 1){
					this.fill10LitersOfFuel();
					continue;
				}else if (input == 2){
					break;
				}
							
			}	
		System.out.println("car has traveled " + userDistance + "km!");
	}
	

	@Override
	public void increaseSpeed() {
		try {
			wheel.setSpeed(10);
			System.out.println("speed is increased by 10 km/h");
		} catch (SpeedingCarException e) {
			System.out.println("Attention! You have exceeded the speed!");
		}
		
	}

	@Override
	public void decreaseSpeed() {
		try {
			wheel.setSpeed(-10);
			System.out.println("speed is decreased by 10 km/h");
		} catch (SpeedingCarException e) {
		
			System.out.println("Attention! You stopped!");
		}
		
	}

	@Override
	public void showRemainingFuel() {
		System.out.println("fuel remaining - " + tank.getFuelTank());	
	}

	@Override
	public void fill10LitersOfFuel() {
		try {
			tank.setFuel(10);
			System.out.println("fueled 10 liters of fuel");
		} catch (OverflowGasTankCarException e) {
			System.out.println("Attention! Gas tank overflowing!");
		}
		
	}

}
