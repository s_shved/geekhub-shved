package lessons3.car;

public interface Driveable {

	void travel();
	void increaseSpeed();
	void decreaseSpeed();
	void showRemainingFuel();
	void fill10LitersOfFuel();
	
}
