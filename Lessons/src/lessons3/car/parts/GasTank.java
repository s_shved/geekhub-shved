package lessons3.car.parts;

import lessons3.car.exception.*;



public class GasTank {

	public static final int VOLUME_OF_THE_TANK = 50;
	private int fuel = 20;
	
	public int getFuel(int f) throws NotEnoughFuelException {
		fuel = fuel - f;
		if (this.fuel <= 0)
			throw new NotEnoughFuelException();
		return f;
	}
	
	public int getFuelTank() {
		return fuel;
	}
	
	public void setFuel(int fuel) throws OverflowGasTankCarException {
		this.fuel = this.fuel + fuel;
		if (this.fuel > VOLUME_OF_THE_TANK)
			throw new OverflowGasTankCarException();
			
	}
	
	
}
