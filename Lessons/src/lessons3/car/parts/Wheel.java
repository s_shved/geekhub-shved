package lessons3.car.parts;

import lessons3.car.exception.SpeedingCarException;

public class Wheel {

	public static final int MAX_SPEED = 80;
	private int speed = 60;
	
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) throws SpeedingCarException {
		this.speed = this.speed + speed;
		if (this.speed > MAX_SPEED || this.speed < 0)
			throw new SpeedingCarException();
	}
	
	public double goCar(double torque, int speed){
		double distance = torque;
		System.out.println("car has traveled " + distance + " kilometers at a speed of " + speed + "km/h");
		return distance;
	}
	
}
