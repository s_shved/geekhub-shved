package lessons3.car.parts;

public class Engine {
	
	public static final double FUEL_CONSUMPTION_PER_100_KM = 1;
	
	public double startEngine(int fuel){
		return fuel / FUEL_CONSUMPTION_PER_100_KM; 
	}

}
