package lessons3.car;

import java.util.Scanner;

public class Main {

	private static Scanner input;
	
	public static void main(String[] args) {
		Driveable car = new Car();
		System.out.println("Hello! You are using a fine car."); 
		while (true) {
            actionWithVehicle(chooseAction(), car);
        }

	}

	 private static int chooseAction() {
	        System.out.println("1 - Travel");
	        System.out.println("2 - Increase speed on 10km/h");
	        System.out.println("3 - Decrease speed on 10km/h");
	        System.out.println("4 - To show the remaining fuel");
	        System.out.println("5 - Fill 10 liters of fuel");
	        System.out.println("6 - Exit");
	        System.out.println("Please select the action:");
	        input = new Scanner(System.in);
	        return input.nextInt();
	    }
	
	 private static void actionWithVehicle(int actionNumber, Driveable car) {
	        switch (actionNumber) {
	            case 1:
	                car.travel();
	                break;
	            case 2:
	                car.increaseSpeed();
	                break;
	            case 3:
	                car.decreaseSpeed();
	                break;
	            case 4:
	                car.showRemainingFuel();
	                break;
	            case 5:
	                car.fill10LitersOfFuel();
	                break;
	            case 6:
	            	System.exit(0);
	        }
	    }
}
