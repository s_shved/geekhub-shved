package lessons3.comparable;

public class Book implements Comparable {
	
	String author;
	String title;
	
	public Book (String author, String title){
		this.author = author;
		this.title = title;
	}



	@Override
	public int compareTo(Object o) {
		 Book anotherBook = (Book) o ;
	        if (anotherBook.author.charAt(0)!= this.author.charAt(0)) {
	            if (this.author.charAt(0) < anotherBook.author.charAt(0)) {
	                return -1;
	            } else {
	                return 1;
	            }
	        }
	        return 0;
	}

	@Override
    public String toString(){
        return author + " - " + title;
    }
	
}
