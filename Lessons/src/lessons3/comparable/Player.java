package lessons3.comparable;


	public class Player implements Comparable {

	    public int level;
	    public String name;

	    public Player (int level, String name){
	        this.level = level;
	        this.name = name;
	    }

	    @Override
	    public int compareTo(Object o) {
	        Player anotherPlayer = (Player) o ;
	        if (anotherPlayer.level!= this.level) {
	            if (this.level< anotherPlayer.level) {
	                return -1;
	            } else {
	                return 1;
	            }
	        }
	        return 0;
	    }

	    @Override
	    public String toString(){
	        return name  + " - " + String.valueOf(this.level);
	    }

	}
	

