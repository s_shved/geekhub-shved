package lessons3.comparable;

import java.util.Arrays;


public class Main {

	public static void main(String[] args) {

		        Main m = new Main();
		        
		        System.out.println("Test 'comparable' #1:");
		        Player[] players = new Player[5];
		        players[0] = new Player(22, "����");
		        players[1] = new Player(13, "�����");
		        players[2] = new Player(88, "�����");
		        players[3] = new Player(6, "����");
		        players[4] = new Player(45, "������");
		        System.out.println("Original array 'Player[]' before sorting:");
		        for(Player p : players){
		             System.out.println(p);
		        }
		        Player[] newPlayers = (Player[]) m.sort(players);
		        System.out.println("\nOriginal array 'Player[]' after sorting by 'level':");
		        for(Player p : players){
		            System.out.println(p);
		        }
		        System.out.println("\nCopy array 'Player[]' after sorting by 'level':");
		        for(Player p : newPlayers){
		            System.out.println(p);
		        }
		        
		        System.out.println("\nTest 'comparable' #2:");
		        Book[] books = new Book[5];
		        books[0] = new Book("��������", "������");
		        books[1] = new Book("������", "ǳ�'��� �����");
		        books[2] = new Book("��������", "г�� ��������");
		        books[3] = new Book("��������", "���������");
		        books[4] = new Book("������������", "��������");
		        System.out.println("Original array 'Book[]' before sorting:");
		        for(Book b : books){
		             System.out.println(b);
		        }
		        Book[] newBooks = (Book[]) m.sort(books);
		        System.out.println("\nOriginal array 'Book[]' after sorting by 'level':");
		        for(Book b : books){
		             System.out.println(b);
		        }
		        System.out.println("\nCopy array 'Book[]' after sorting by 'level':");
		        for(Book b : newBooks){
		             System.out.println(b);
		        }
		    }

		    Comparable[] sort(Comparable[] elemens) {
		    	Comparable[] newPlayers = (Comparable[])elemens.clone();
		        Arrays.sort(newPlayers);
		        return newPlayers;
		    }


	}


