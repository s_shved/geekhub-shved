package lessons3.testString;

public class TestPerformanceString {
	
	public static final int NUMBER_CONCATENATION = 100000;
	
	public static void main(String[] args) {
		
		TestPerformanceString test = new TestPerformanceString();
		test.testString();
		test.testStringBuilder();
		test.testStringBuffer();

	}

	void testString(){
		System.out.println("Test the performance of the method String concatenation:");
		long timeStart = System.currentTimeMillis();
		String str = "geek";
		for (int i = 0; i < NUMBER_CONCATENATION; ++i){
			str = str + "hub";
		}
		System.out.println("Working time method testString - " + (System.currentTimeMillis() - timeStart));
	}
	
	void testStringBuilder(){
		System.out.println("Test the performance of the method StringBuilder concatenation:");
		long timeStart = System.currentTimeMillis();
		StringBuilder str = new StringBuilder("geek");
		for (int i = 0; i < NUMBER_CONCATENATION; ++i){
			str.append("hub");
		}
		System.out.println("Working time method testStringBuilder - " + (System.currentTimeMillis() - timeStart));
	}
	
	void testStringBuffer(){
		System.out.println("Test the performance of the method StringBuffer concatenation:");
		long timeStart = System.currentTimeMillis();
		StringBuffer str = new StringBuffer("geek");
		for (int i = 0; i < NUMBER_CONCATENATION; ++i){
			str.append("hub");
		}
		System.out.println("Working time method testStringBuffer - " + (System.currentTimeMillis() - timeStart));
	}
	
}
