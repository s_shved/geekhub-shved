package lessons6.json.adapters;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {
    @Override
    public Object toJson(Collection c) throws JSONException{
        JSONArray arrays = new JSONArray();
        for (Object object : c) {
            arrays.put(object);
        }
        return arrays;
    }
}
