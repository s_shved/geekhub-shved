package lessons4.taskManager;

import java.util.Calendar;

public class Main {
    public static void main(String[] args){
        TestTaskManager test = new TestTaskManager();
        Calendar calendar = Calendar.getInstance();
        calendar.add(calendar.DATE, -5);
        test.addTask(calendar.getTime(), new Task("category #2", "task #1"));
        calendar.add(calendar.DATE, 1);
        test.addTask(calendar.getTime(), new Task("category #1", "task #1"));
        calendar.add(calendar.DATE, 1);
        test.addTask(calendar.getTime(), new Task("category #2", "task #2"));
        calendar.add(calendar.DATE, 1);
        test.addTask(calendar.getTime(), new Task("category #2", "task #3"));
        calendar.add(calendar.DATE, 1);
        test.addTask(calendar.getTime(), new Task("category #1", "task #2"));
        calendar.add(calendar.DATE, 1);
        test.addTask(calendar.getTime(), new Task("category #1", "task #3"));

        System.out.println("Исходный контейнер 'tasks':");
        System.out.println(test.getTasks());
        System.out.println("Метод 'getCategories':");
        System.out.println(test.getCategories());
        System.out.println("Метод 'getTasksByCategories':");
        System.out.println(test.getTasksByCategories());
        System.out.println("Метод 'getTasksByCategory':");
        System.out.println(test.getTasksByCategory("category #1"));
        System.out.println("Метод 'getTasksForToday':");
        System.out.println(test.getTasksForToday());
        System.out.println("end=)");
    }
}
