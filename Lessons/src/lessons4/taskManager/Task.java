package lessons4.taskManager;

public class Task {

    private String category;
    private String description;

    public Task (String category, String description){
        this.category = category;
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString(){
        return category + ", " + description;
    }

}
