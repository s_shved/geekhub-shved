package lessons4.taskManager;

import java.util.*;

public class TestTaskManager implements TaskManager{

    private SortedMap<Date, Task> tasks = new TreeMap<Date, Task>();

    public Map<Date, Task> getTasks() {
        return tasks;
    }


    @Override
    public void addTask(Date date, Task task) {
        tasks.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        tasks.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Collection<String> categories = new HashSet<String>();
        for(Task task : tasks.values())
            categories.add(task.getCategory());
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> mapTasksByCategories = new HashMap<String, List<Task>>();
        for (String category : getCategories())
            mapTasksByCategories.put(category, getTasksByCategory(category));
        return mapTasksByCategories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> listTasksByCategory = new ArrayList<Task>();
        for (Task task: tasks.values())
            if(task.getCategory().equals(category))
                listTasksByCategory.add(task);
        return listTasksByCategory;
    }

    @Override
    public List<Task> getTasksForToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        ArrayList<Task> tasksForToday = new ArrayList<Task>();
        ArrayList<Date> keys = new ArrayList<Date>();
        for(Date key : tasks.keySet())
            if(key.after(calendar.getTime()))
                 keys.add(key);
        for(Date key : keys)
            tasksForToday.add(tasks.get(key));

        return tasksForToday;
    }
}
