package lessons4.setOperations;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args){

        Set a = new HashSet();
        for(int i = 0; i < 10; ++i){
            a.add((int) (Math.random() * 15));
        }
        System.out.println("Множество А:");
        System.out.println(a);

        Set b = new HashSet();
        for(int i = 0; i < 10; ++i){
            b.add((int) (Math.random() * 15));
        }
        System.out.println("Множество В:");
        System.out.println(b);

        SetOperations test = new TestSetOperations();

        if(test.equals(a, b)){
            System.out.println("Mножества А и В равны (А=В)");
        } else {
            System.out.println("Mножества А и В не равны");
        }
        System.out.println("Объединение (сумма) множеств А и В (A ∪ B):");
        System.out.println(test.union(a, b));

        System.out.println("Разность множеств А и В (AB):");
        System.out.println(test.subtract(a, b));

        System.out.println("Пересечение (произведение) множеств А и В (А ∩ В):");
        System.out.println(test.intersect(a, b));

        System.out.println("Симметричная разностью множеств А и В (А Δ В):");
        System.out.println(test.symmetricSubtract(a, b));

        System.out.println("end=)");
    }
}
