package lessons4.setOperations;

import java.util.HashSet;
import java.util.Set;

public class TestSetOperations implements SetOperations{

    @Override
    public boolean equals(Set a, Set b) {
       return a.equals((Object)b);
    }

    @Override
    public Set union(Set a, Set b) {
        Set aUb = new HashSet();
        aUb.addAll(a);
        aUb.addAll(b);
        return aUb;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set ab = new HashSet();
        ab.addAll(a);
        for(Object x : a){
           for(Object y : b){
               if(x.equals(y))
                   ab.remove(x);
           }
        }
        return ab;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set aIb = new HashSet();
        for(Object x : a){
            for(Object y : b){
                if(x.equals(y))
                    aIb.add(x);
            }
        }
        return aIb;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set abUba = new HashSet();
        abUba.addAll(a);
        for(Object x : a){
            for(Object y : b){
                if(x.equals(y))
                    abUba.remove(x);
            }
        }
        Set ba = new HashSet();
        ba.addAll(b);
        for(Object x : b){
            for(Object y : a){
                if(x.equals(y))
                    ba.remove(x);
            }
        }
        abUba.addAll(ba);
        return abUba;
    }
}
