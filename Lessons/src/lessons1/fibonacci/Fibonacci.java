package lessons1.fibonacci;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Fibonacci {

    public static void main (String[] args){
        System.out.println("This program generates the Fibonacci sequence.");
        Fibonacci fib = new Fibonacci();
        fib.createFibonacci();
        System.out.println("end:)");
    }

    // формирует и выводин в консоль последовательность Фибоначчи
    private void createFibonacci(){
        int a0 = 0, a1 = 1, a2;
        int userInputN = this.setUserInput() - 2;
       if(userInputN == -2){
           System.out.println("empty");
       } else if (userInputN == -1){
           System.out.println(a0);
       }  else {
            System.out.print(a0+" "+a1+" ");
            for(int i = 0; i < userInputN; i++){
                a2=a0+a1;
                System.out.print(a2+" ");
                a0=a1;
                a1=a2;
            }
        System.out.println();
    }
    }
    // получает пользовательский ввод, проверяет его коректность
    // и возвращает его приведеным к int
    private int setUserInput(){
        String userInput = "0";
        do {System.out.print("Please Enter a positive integer: ");
            BufferedReader input = new BufferedReader(new  InputStreamReader(System.in));
            try {
                userInput = input.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        while(!checkUserInput(userInput));
        return Integer.parseInt(userInput);
    }

    // проверяет коректность пользовательского ввода регулярным выражениям
    // возвращает try если ввод коректный
    private Boolean checkUserInput(String userInput){
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(userInput);
        return m.matches();
    }
}
