package lessons1.factorials;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Factorial {

    private String stringUserInput; // хранит значение пользовательского ввода в строке
    private int intUserInput; // хранит значение пользовательского ввода целым числом
    private BigInteger factorialIntUserInput = BigInteger.ONE; // хранит факториал переменной intUserInput

    public static void main (String[] args) {
        System.out.println("Hi, this program counts the factorial of a positive integer.");
        Factorial fact = new Factorial();
        fact.setIntUserInput();
        fact.calcFactorial();
        System.out.println("end:)");
    }

    // получает пользовательский ввод и присваевает его переменной stringUserInput
    // проверяет его коректность, если коректный то инициализирует им переменную intUserInput
    private void setIntUserInput() {
        do {System.out.print("Please Enter a positive integer: ");
            BufferedReader input = new BufferedReader(new  InputStreamReader(System.in));
            try {
                stringUserInput = input.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        while(!checkStringUserInput());
        intUserInput = Integer.parseInt(stringUserInput);
    }

    // проверяет коректность пользовательского ввода регулярным выражениям
    // возвращает try если ввод коректный
    private Boolean checkStringUserInput(){
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(stringUserInput);
        return m.matches();
    }

    // считает факториал и присваевает его значение переменной factorialIntUserInput
    // выводит результ в консоль
    private void calcFactorial(){
        for (int i = 1; i <= intUserInput; ++i)
            factorialIntUserInput = factorialIntUserInput.multiply(BigInteger.valueOf(i));
        System.out.println(stringUserInput + "! = " + factorialIntUserInput.toString());
    }

}
