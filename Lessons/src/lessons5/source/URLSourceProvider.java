package lessons5.source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            URL url = new URL(pathToSource);
            return true;
        } catch (MalformedURLException e) {
            return false;        }
    }

    @Override
    public String load(String pathToSource) throws IOException {
        URL url = new URL(pathToSource);
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder source = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null){
            source.append(line);
        }
        reader.close();
        return source.toString();
    }
}
