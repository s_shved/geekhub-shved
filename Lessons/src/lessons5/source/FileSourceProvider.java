package lessons5.source;

import java.io.*;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File myFile = new File(pathToSource);
        return myFile.canRead();
    }

    @Override
    public String load(String pathToSource) throws IOException {
        File myFile = new File(pathToSource);
        BufferedReader reader = new BufferedReader(new FileReader(myFile));
        StringBuilder source = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null){
            source.append(line);
        }
        reader.close();
        return source.toString();
    }
}
