package lessons2;

class SolarBattery implements EnergyProvider{

	private int fuel;
	
	public SolarBattery(){
		fuel = 200;
		System.out.println("battery is charged");
	}
	
	@Override
	public int consumeFuel(int f) {
		if (fuel > 0){
			fuel =- f;
			return f;
		} else {
			System.out.println("battery is low");
			return 0;
		}
	}
	
}
