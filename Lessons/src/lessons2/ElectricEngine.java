package lessons2;

class ElectricEngine implements ForceProvider{

	@Override
	public int startEngine(int fuel){
		return fuel * 1000; // transmits the torque
	}
	
}
