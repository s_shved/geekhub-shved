package lessons2;

public interface ForceProvider {

	int startEngine(int fuel);
	
}
