package lessons2;

public interface EnergyProvider {

	public int consumeFuel(int f);
	
}
