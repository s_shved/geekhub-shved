package lessons2;

class SolarPoweredCar extends Car implements Driveable {

	EnergyProvider battery= new SolarBattery();
	ForceProvider engine = new ElectricEngine();
	ForceAcceptor wheel = new Wheel();
	
	@Override
	public void accelerate() {
		wheel.goVehicle(engine.startEngine(battery.consumeFuel(TEST_AMOUNT_FUEL)));
	}

	@Override
	public void brake() {
		wheel.stopVehicle();
	}

	@Override
	public void turn() {
		wheel.turnVehicle();
	}

}
