package lessons2;

public class Main {

	public static void main(String[] args) {

        TestDriveable test = new TestDriveable();

        System.out.println("#1 test petrol car");
        Driveable petrolCar = new PetrolCar();
        test.testDrive(petrolCar);

        System.out.println("\n#2 test solar-powered car");
        Driveable solarPoweredCar = new SolarPoweredCar();
        test.testDrive(solarPoweredCar);
        
        System.out.println("\n#3 test boat");
        Driveable boad = new Boat();
        test.testDrive(boad);

	}

}
