package lessons2;

class Boat extends Vehicle implements Driveable{

	EnergyProvider tank = new GasTank();
	ForceProvider engine = new DieselEngine();
	ForceAcceptor propeller = new Propeller();
	
	@Override
	public void accelerate() {
		propeller.goVehicle(engine.startEngine(tank.consumeFuel(TEST_AMOUNT_FUEL)));
	}

	@Override
	public void brake() {
		propeller.stopVehicle();
	}

	@Override
	public void turn() {
		propeller.turnVehicle();
	}

}
