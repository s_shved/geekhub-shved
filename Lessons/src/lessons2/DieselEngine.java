package lessons2;

class DieselEngine implements ForceProvider{

	@Override
	public int startEngine(int fuel){
		return fuel * 2000; // transmits the torque
	}
	
}
