package lessons2;

class Propeller implements ForceAcceptor {

	@Override
	public void goVehicle(int torgue){
		System.out.println("Boat goes at a speed of " + torgue/500 + "km/h" );
	}

	@Override
	public void stopVehicle() {
		System.out.println("Boat brake");
		
	}

	@Override
	public void turnVehicle() {
		System.out.println("Boat turn");
		
	}
}
