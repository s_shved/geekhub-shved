package lessons2;

class GasTank implements EnergyProvider {
	
	private int fuel;
	
	public GasTank(){
		fuel = 100;
		System.out.println("full tank");
	}
	
	@Override
	public int consumeFuel(int f) {
		if (fuel > 0){
			fuel =- f;
			return f;
		} else {
			System.out.println("empty tank");
			return 0;
		}
	}
}
