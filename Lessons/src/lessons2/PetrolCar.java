package lessons2;

class PetrolCar extends Car implements Driveable {

	EnergyProvider tank = new GasTank();
	ForceProvider engine = new DieselEngine();
	ForceAcceptor wheel = new Wheel();
	
	@Override
	public void accelerate() {
		wheel.goVehicle(engine.startEngine(tank.consumeFuel(TEST_AMOUNT_FUEL)));
	}

	@Override
	public void brake() {
		wheel.stopVehicle();
	}

	@Override
	public void turn() {
		wheel.turnVehicle();
	}


}
