package lessons2;

interface Driveable {

	void accelerate();
	void brake();
	void turn();
	
}
