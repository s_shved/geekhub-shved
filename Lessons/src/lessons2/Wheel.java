package lessons2;

class Wheel implements ForceAcceptor {
	
	@Override
	public void goVehicle(int torgue){
		System.out.println("Car goes at a speed of " + torgue/200 + "km/h" );
	}

	@Override
	public void stopVehicle() {
		System.out.println("Car brake");
		
	}

	@Override
	public void turnVehicle() {
		System.out.println("Car turn");
		
	}
	
}
