package lessons2;

public interface ForceAcceptor {
	
	void goVehicle(int torgue);
	void stopVehicle();
	void turnVehicle();
	
}
