package model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Data {
    private Map<String, List<String>> items = new LinkedHashMap<>();

    public Map<String, List<String>> getItems() {
        return items;
    }

    public void put(String key, String value) {
        if (items.containsKey(key)) {
            items.get(key).add(value);
        } else {
            List<String> list = new ArrayList<>();
            list.add(value);
            items.put(key, list);
        }
    }

    public void remove(String key, String value) {
        if (items.containsKey(key) && items.containsValue(value)) {
            if (items.get(key).size() > 1) {
                items.get(key).remove(value);
            } else {
                items.remove(key);
            }
        }
    }
}
