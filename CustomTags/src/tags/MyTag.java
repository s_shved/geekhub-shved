package tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.util.Iterator;
import java.util.Map;

public class MyTag extends BodyTagSupport {
    private Map map;
    private String key;
    private String value;
    private Iterator iterator;
    Map.Entry entry;

    public void setMap(Map map) {
        this.map = map;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int doStartTag() throws JspException {
        if (!map.isEmpty()) {
            iterator = map.entrySet().iterator();
            doBody();
            return EVAL_BODY_INCLUDE;
        } else {
            return SKIP_BODY;
        }
    }

    @Override
    public int doAfterBody() throws JspException {
        if (iterator.hasNext()) {
            doBody();
            return EVAL_BODY_AGAIN;
        } else {
            return SKIP_BODY;
        }
    }

    private void doBody() {
        entry = (Map.Entry) iterator.next();
        pageContext.setAttribute(key, entry.getKey());
        pageContext.setAttribute(value, entry.getValue());
    }

}
