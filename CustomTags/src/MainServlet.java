import model.Data;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Main", urlPatterns = {"/"})
public class MainServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Data data = (Data) request.getSession().getAttribute("data");
        String name = request.getParameter("name");
        String value = request.getParameter("value");

        if ((data != null) && !"".equals(name) && !"".equals(value)) {
            data.put(name, value);
        }
        response.sendRedirect("./");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/view/index.jsp").forward(request, response);
    }
}
