import model.Data;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DataRemover", urlPatterns = {"/delete"})
public class RemoverServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Data data = (Data) request.getSession().getAttribute("data");
        String name = request.getParameter("name");
        String value = request.getParameter("value");

        if ((data != null) && (name != null) && (value != null)) {
            data.remove(name, value);
        }
        response.sendRedirect("/");
    }
}
