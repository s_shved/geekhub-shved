<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="shved" uri="http://geekhub.com/castom-tag" %>
<jsp:useBean id="data" class="model.Data" scope="session"/>
<!DOCTYPE html>
<html>
<head>
    <title>Homework #10</title>
</head>
<body>
<form id="form" method="post"></form>
<table border="1">
    <thead>
    <tr>
        <th>Name</th>
        <th>Value</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <td><input type="text" form="form" name="name" required/></td>
        <td><input type="text" form="form" name="value" required/></td>
        <td>
            <button type="submit" form="form">add</button>
        </td>
    </tr>
    </tfoot>
    <shved:map map="${data.items}" key="key" value="value">
        <tbody>
        <c:forEach var="item" items="${value}" varStatus="current">
            <tr>
                <c:choose>
                    <c:when test="${current.first}">
                        <td>${key}</td>
                    </c:when>
                    <c:otherwise>
                        <td></td>
                    </c:otherwise>
                </c:choose>
                <td>
                        ${item}
                </td>
                <td>
                    <a href="./delete?name=${key}&value=${item}">delete</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </shved:map>
</table>
</body>
</html>